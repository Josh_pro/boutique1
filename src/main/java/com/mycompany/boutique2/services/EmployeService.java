/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.boutique2.services;

import com.mycompany.boutique2.entites.Employe;
import java.time.LocalDate;
import java.time.Month;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author a
 */
public class EmployeService {
    
    private static final List<Employe> liste = new ArrayList<>();
    private static EmployeService INSTANCE;
    
    public EmployeService() {
        Employe e1 = new Employe("VBEI", 1, "AKATO", "Massan Kossiwa", LocalDate.of(1989, Month.APRIL, 12));
        Employe e2 = new Employe("BNCE", 2, "KAWOUDJI", "kodjo Marc", LocalDate.of(1993, Month.MARCH, 3));
        Employe e3 = new Employe("FHKE", 3, "HADJIM", "komla Venunye", LocalDate.of(1996, Month.AUGUST, 5));
        liste.add(e1);
        liste.add(e2);
        liste.add(e3);
    }
    
    public static synchronized EmployeService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EmployeService();
        }
        return INSTANCE;
    }
    
    public List<Employe> lister() {
        return liste;
    }
    
    public Employe trouver(long id) {
        for (Employe employe : liste) {
           if (employe.getId() == id) {
               return employe;
           }
       }
       return null;
    }
    
    public int compter() {
        return liste.size();
    }
    
    public void ajouter(Employe e) {
        liste.add(e);
    }
    
    public void modifier (Employe e) {
        int i= -1;
        
        for (Employe employe : liste) {
            i++;
            if (employe.equals(e)) {
                break;
            }
        }
        
        if (i >= 0) {
            liste.set(i, e);
        }
    }
    
    public void supprimer(long id) {
        liste.remove(id);
    }
}
