/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.boutique2.services;

import com.mycompany.boutique2.entites.Categorie;
import com.mycompany.boutique2.entites.Produit;
import com.mycompany.boutique2.entites.ProduitAchete;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author a
 */
public class ProduitAcheteService {
    private static ArrayList<ProduitAchete> liste = new ArrayList<>();
    private static ProduitAcheteService INSTANCE;
    
    private ProduitAcheteService() {        
        Categorie cat1 = new Categorie(2, "Robes", "Ovale, tunique, ...");
        
        Produit p1 = new Produit(1l, "Fila", 1000.0, LocalDate.of(2025, 12, 1), cat1);
        Produit p2 = new Produit(2l, "Adidas", 2500.0, LocalDate.of(2030, 12, 1), cat1);
        
        liste.add(new ProduitAchete(15, 0.5, p1));
        liste.add(new ProduitAchete(24, 0.1, p2));
    }
            
   public static synchronized ProduitAcheteService getInstance() {
       if (INSTANCE == null) {
           INSTANCE = new ProduitAcheteService();
       }
       return INSTANCE;
   }
   
   public List<ProduitAchete> lister() {
       return liste;
   }
   
   public int compter () {
       return liste.size();
   }
   
   public void ajouter(ProduitAchete p) {
       liste.add(p);
   }
   
   public void modifier(ProduitAchete p) {
       int i = -1;
       
       for (ProduitAchete produitAch : liste) {
           i++;
           if (produitAch.equals(p)) {
               break;
           }
       }
       
       if (i >= 0) {
           liste.set(i, p);
       }
   }
  
   public ProduitAchete trouver(long id) {
       return null;
   }
   
   public void supprimer (long id){
       liste.remove(id);
   }
}
