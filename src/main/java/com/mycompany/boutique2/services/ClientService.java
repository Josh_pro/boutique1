/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.boutique2.services;

import com.mycompany.boutique2.entites.Client;
import com.mycompany.boutique2.entites.Produit;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author a
 */
public class ClientService {
    
    private static List<Client> liste = new ArrayList<>();
    private static ClientService INSTANCE;
    
    private ClientService() {
        Client c1 = new Client("EFEV","ROLCZ",1,"AYIVOR","Kossi Mawulolo", LocalDate.of(1993, 12, 4));
        Client c2 = new Client("NECK","AJDCZ",2,"SAQUI","Kofi Joe", LocalDate.of(1992, 2, 1));
        liste.add(c1);
        liste.add(c2);
    }
    
    public static synchronized ClientService getInstance() {
       if (INSTANCE == null) {
           INSTANCE = new ClientService();
       }
       return INSTANCE;
   }
    
    public List<Client> lister () {
       return liste;
   }
   
   public Client trouver(long id) {
       
       for (Client client : liste) {
           if (client.getId() == id) {
               return client;
           }
       }
       return null;
   }
   
   public int compter() {
       return liste.size();
   }
   
   public void ajouter(Client c) {
       liste.add(c);
   }
   
   public void modifier(Client c) {
       int i = -1;
       
       for (Client client : liste) {
           i++;
           if (client.equals(c)) {
               break;
           }
       }
       
       if (i >= 0) {
           liste.set(i, c);
       }
   }
   
   public void supprimer (long id){
       liste.remove(id);
   }
    
}
