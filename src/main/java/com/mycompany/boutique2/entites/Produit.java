/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.boutique2.entites;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Classe des Produits
 */
public class Produit {
    private long id;
    private String libelle;
    private double prixUnitaire;
    private LocalDate datePeremption;
    private Categorie categorie;
    
    public Produit() {
    }

    public Produit(long id, String libelle, double prixUnitaire, LocalDate datePeremption, Categorie c) {
        this.id = id;
        this.libelle = libelle;
        this.prixUnitaire = prixUnitaire;
        this.datePeremption = datePeremption;
        this.categorie = c;
    }
    
    public boolean estPerime() {
        return false;
    }
    
    public boolean estPerime(LocalDate dateReference) {
        return false;
    }

    //Les accesseurs
    public void setId(long id) { this.id = id; }

    public long getId() { return id; }

    public void setLibelle(String libelle) { this.libelle = libelle;   }

    public String getLibelle() { return libelle; }

    public void setPrixUnitaire(double prixUnitaire) { this.prixUnitaire = prixUnitaire; }

    public double getPrixUnitaire() { return prixUnitaire; }

    public void setDatePeremption(LocalDate datePeremption) { this.datePeremption = datePeremption; }

    public LocalDate getDatePeremption() { return datePeremption; }
    
    //les methodes de la classe Object
    @Override
    public boolean equals(Object obj) { 
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
     }
    
    @Override
    public int hashCode() {
        return Objects.hash(getId()); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
