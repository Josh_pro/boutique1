/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.boutique2.entites;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author Super Classe Personne dont herite les classe Employe et Client
 */
public class Client extends Personne {
    private String cin;
    private String carteVisa;

    //Le constructeur
    public Client(String cin, String carteVisa, long id, String nom, String prenoms, LocalDate dateNaissance) {
        super(id, nom, prenoms, dateNaissance);
        this.cin = cin;
        this.carteVisa = carteVisa;
    }

    //Les accesseurs
    public void setCin(String cin) { this.cin = cin; }

    public String getCin() { return cin; }

    public void setCarteVisa(String carteVisa) { this.carteVisa = carteVisa; }

    public String getCarteVisa() { return carteVisa; }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if(!(obj instanceof Client)) return false;
        
        Client c1 = (Client) obj;
        
        
        return getNom().equalsIgnoreCase(c1.getNom()) && 
               getPrenoms().equalsIgnoreCase(c1.getPrenoms()) &&
               cin.equalsIgnoreCase(c1.getCin()) &&
               carteVisa.equalsIgnoreCase(c1.getCarteVisa());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Nom : \n" +getNom()+ "\nPrenoms : "+getPrenoms()+"\nDate de Naissance : "+getDateNaissance()
                +"\nCIN : "+getCin()+"\nCarte Visa : "+getCarteVisa();
    }
    
}
