/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.boutique2.entites;

import java.time.LocalDate;

/**
 *
 * @author a
 */
public abstract class Personne {
    private long id;
    private String nom;
    private String prenoms;
    private LocalDate dateNaissance;

    public Personne(long id, String nom, String prenoms, LocalDate dateNaissance) {
        this.id = id;
        this.nom = nom;
        this.prenoms = prenoms;
        this.dateNaissance = dateNaissance;
    }
    
    public int getAge() {
        return 0;
    }
    
    public int getAge(LocalDate dateReference) {
        return 0;
    }
    
    //Les accesseurs

    public void setId(long id) { this.id = id; }

    public long getId() { return id; }

    public void setNom(String nom) { this.nom = nom; }

    public String getNom() { return nom; }

    public void setPrenoms(String prenoms) { this.prenoms = prenoms; }

    public String getPrenoms() { return prenoms; }

    public void setDateNaissance(LocalDate dateNaissance) { this.dateNaissance = dateNaissance; }

    public LocalDate getDateNaissance() { return dateNaissance;  }
    
}

