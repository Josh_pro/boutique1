/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.boutique2.entites;

/**
 *
 * @author Classe Categorie des produits
 */
public class Categorie {
    private int id;
    private String libelle;
    private String description;
    
    //Constructeur par défaut
    public Categorie() { }
    
    public Categorie(int id, String lib, String desc) {
        this.id = id;
        this.libelle = lib;
        this.description = desc;
    }

    //Les accesseurs
    public void setId(int id) { this.id = id; }

    public int getId() { return id; }

    public void setLibelle(String libelle) { this.libelle = libelle; }

    public String getLibelle() { return libelle; }

    public void setDescription(String description) { this.description = description; }

    public String getDescription() { return description; }
    
    //Les methodes de la classe object 
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
