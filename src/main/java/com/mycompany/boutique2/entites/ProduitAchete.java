/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.boutique2.entites;

/**
 *
 * @author 
 *          Classe des produits achetés avec leurs qté et leurs remises
 */
public class ProduitAchete {
    private int quantite;
    private double remise;
    private Produit produit;
    //private Achat achat;

    //le Constructeur
    public ProduitAchete(int quantite, double remise, Produit pdt) {
        this.quantite = quantite;
        this.remise = remise;
        this.produit = pdt;
        //this.achat = acht;   
    }
    
    //Les accesseurs
    public double getPrixTotale() { 
        double PSR = this.produit.getPrixUnitaire() * this.quantite;
        double PAR = PSR - (PSR * this.remise);
        return PAR; 
    }

    public void setQuantite(int quantite) { this.quantite = quantite; }

    public int getQuantite() { return quantite; }

    public void setRemise(double remise) { this.remise = remise; }

    public double getRemise() { return remise; }
    
    public Produit getProduit() {
        return this.produit;
    }
    

    //les methodes de la classe Object 
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
