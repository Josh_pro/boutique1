/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.boutique2.entites;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Class Employe derivé de la super class personne
 */
public class Employe extends Personne{
    private String cnss;
    
    public Employe(String cnss, long id, String nom, String prenoms, LocalDate dateNaissance) {
        super(id, nom, prenoms, dateNaissance);
        this.cnss = cnss;
    }

    //Les accesseurs
    public void setCnss(String cnss) { this.cnss = cnss; }

    public String getCnss() { return cnss; }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if(!(obj instanceof Employe)) return false;
        
        Employe e1 = (Employe) obj;
        
        
        return getNom().equalsIgnoreCase(e1.getNom()) && 
               getPrenoms().equalsIgnoreCase(e1.getPrenoms()) &&
               cnss.equalsIgnoreCase(e1.getCnss()) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Nom : " +getNom()+ "\nPrenoms : "+getPrenoms()+"\nDate de Naissance : "+getDateNaissance()
                +"\nN_CNSS : "+getCnss();
    }
    
}
