/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.boutique2.entites;

import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author Classe Achat des Achats faits par le client
 */
public class Achat {
    private Long id;
    private LocalDate dateAchat;
    private double remise;
    private Employe employe;
    private Client client;
    private List<ProduitAchete> produit;
    
    //Le constructeur par défaut
    public Achat() {      
    }
    
    
    public Achat(Long id, LocalDate dateAchat, double remise, Employe emp, Client cl, List<ProduitAchete> listPdt_Acht) {
        this.id = id;
        this.dateAchat = dateAchat;
        this.remise = remise;
        this.employe = emp;
        this.client = cl;
        this.produit = listPdt_Acht;
    }
    
    public double getRemiseTotale() { 
        double R = 0.0;
        double RT = 0.0; // Remise Totale
        for (ProduitAchete produitAchete : produit) {
            R += produitAchete.getRemise();
        }
        RT = R *this.remise;
        return RT ; 
    }
    
    public double getPrixTotale() { 
        double PT = 0.0; //Prix Total
        double PTAR = 0.0; //Prix Total Avec Remise
        for (ProduitAchete produitAchete : produit) {
            PT += produitAchete.getPrixTotale();
        }
        
        PTAR = PT*this.remise;
        return PTAR; 
    }

    //Les accesseurs
    public void setId(long id) { this.id = id; }

    public Long getId() { return id; }

    public void setRemise(double remise) { this.remise = remise; }

    public double getRemise() { return remise; }

    public void setDateAchat(LocalDate dateAchat) { this.dateAchat = dateAchat; }

    public LocalDate getDateAchat() { return dateAchat; }

    public Client getClient() {
        return client;
    }

    public Employe getEmploye() {
        return employe;
    }
    
    
   
    //Methodes de la classe object 
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public List<ProduitAchete> getProduit() {
        return this.produit;
    }

    
}
