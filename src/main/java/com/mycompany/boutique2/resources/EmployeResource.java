/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.boutique2.resources;

import com.mycompany.boutique2.entites.Employe;
import com.mycompany.boutique2.services.EmployeService;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author a
 */

@Path("employe")
public class EmployeResource {
    
    private EmployeService service;
    
    public EmployeResource() {
        this.service = EmployeService.getInstance();
    }
    
    @GET
    @Path("/list") 
    public List<Employe> lister() {
        return this.service.lister();
    }
    
    @GET
    @Path("/{id}")
    public Employe trouver(@PathParam("id") long id) {
        return this.service.trouver(id);
    }
    
    @GET
    @Path("/nombre")
    public int compter() {
        return this.service.compter();
    }
    
    @POST 
    public void ajouter(Employe e) {
        this.service.ajouter(e);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") long id) {
        this.service.supprimer(id);
    }
    
}
