/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.boutique2.resources;

import java.util.ArrayList;
import com.mycompany.boutique2.entites.Client;
import com.mycompany.boutique2.entites.Produit;
import com.mycompany.boutique2.services.ClientService;
import com.mycompany.boutique2.services.ProduitService;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author a
 */
@Path("client")
public class ClientResource {
   
    private ClientService service;
    
    public ClientResource (){
        this.service = ClientService.getInstance();
    }
   
    @GET
    @Path("/list")
    public List<Client> lister () {
        return this.service.lister();
    }
    
    @GET
    @Path("/{id}")
    public Client trouver (@PathParam("id") long id) {
        return this.service.trouver(id);
    }
    
    @GET
    @Path("/nombre")
    public int compter () {
        return this.service.compter();
    }
     
    @POST
    public void ajouter(Client c) {
        this.service.ajouter(c);
    }
     
    
    @PUT
    public void modifier(Client c) {
        this.service.modifier(c);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") long id) {
        this.service.supprimer(id);
    }
    
}
