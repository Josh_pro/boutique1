/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.boutique2.resources;

import com.mycompany.boutique2.entites.ProduitAchete;
import com.mycompany.boutique2.services.ProduitAcheteService;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author a
 * @since 05/02/2022
 */
@Path("produitAchete")
public class ProduitAcheteResource {
    
    private ProduitAcheteService service; 
    
    public ProduitAcheteResource() {
        this.service = ProduitAcheteService.getInstance();
    }
    
    @GET
    @Path("/list")
    public List<ProduitAchete> lister() {
        return this.service.lister();
    }
    
    @GET
    @Path("/nombre")
    public int compter() {
        return this.service.compter();
    }
    
    @GET
    @Path("/{id}")
    public ProduitAchete trouver(@PathParam("id") long id) {
        return this.service.trouver(id);
    }
    
    @POST
    public void ajouter(ProduitAchete pa) {
        this.service.ajouter(pa);
    }
    
    @PUT 
    public void modifier(ProduitAchete pa) {
        this.service.modifier(pa);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") long id){
        this.service.supprimer(id);
    }
}
